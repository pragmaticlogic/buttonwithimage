﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace ButtonWithImage
{
	public partial class MainPage : ContentPage
	{
		class PageViewModel : INotifyPropertyChanged
		{
			public event PropertyChangedEventHandler PropertyChanged;
			protected virtual void OnPropertyChanged(string propertyName)
			{
				PropertyChangedEventHandler handler = PropertyChanged;
				if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
			}
			private bool _isLoading;
			public bool IsLoading
			{
				get { return _isLoading; }
				set
				{
					_isLoading = value;
					OnPropertyChanged("IsLoading");
				}
			}
			private bool _noConnection;
			public bool NoConnection
			{
				get { return _noConnection; }
				set 
				{
					_noConnection = value;
					OnPropertyChanged("NoConnection");
				}
			}
		}

		protected Label _btnLabel;
		protected Image _btnImage;
		protected StackLayout _btnStackLayout;

		public MainPage ()
		{
			InitializeComponent ();

			_btnLabel = this.FindByName<Label>("ButtonLabel");

			_btnLabel.GestureRecognizers.Add(new TapGestureRecognizer
				{
					Command = new Command(async () => await OnConnect()),
				});

			_btnImage = this.FindByName<Image>("ButtonImage");
			_btnImage.GestureRecognizers.Add(new TapGestureRecognizer
				{
					Command = new Command(async () => await OnConnect()),
				});

			_btnStackLayout = this.FindByName<StackLayout>("ButtonStackLayout");
			_btnStackLayout.GestureRecognizers.Add(new TapGestureRecognizer
				{
					Command = new Command(async () => await OnConnect()),
				});
		}

		protected async override void OnAppearing()
		{
			base.OnAppearing ();            

			var context = new PageViewModel { 
				IsLoading = false,
				NoConnection = true,
			};
			BindingContext = context;

			await Connect();
		}

		private async Task OnConnect()
		{
			await _btnImage.ScaleTo(0.9, 50, Easing.Linear);
			await Task.Delay(100);
			await _btnImage.ScaleTo(1, 50, Easing.Linear);


			animateButtonTouched(_btnStackLayout, 1500, "#F8F8F8", "#E3E3E3", 1);
			animateButtonTouched(_btnLabel, 1500, "#F8F8F8", "#E3E3E3", 1);
			animateButtonTouched(_btnImage, 1500, "#F8F8F8", "#E3E3E3", 1);

			await Connect();
		}

		private async Task Connect()
		{
			var context = (PageViewModel)BindingContext;
			context.IsLoading = true;
			context.NoConnection = false;
			await Task.Delay (1500);
			context.IsLoading = false;
			context.NoConnection = true;
			/*
			await Task.Delay(2000).ContinueWith((t) => {
				context.IsLoading = false;
				context.NoConnection = true;	
			});*/
		}

		private void animateButtonTouched(View view, uint duration, string hexColorInitial, string hexColorFinal, int repeatCountMax)
		{
			var repeatCount = 0;
			view.Animate ("changedBG", new Animation ((val) => {
				if (repeatCount == 0) {
					view.BackgroundColor = Color.FromHex(hexColorInitial);	
				} else {
					view.BackgroundColor = Color.FromHex(hexColorFinal);
				}
			}), duration, finished: (val, b) => {
				repeatCount++;
			}, repeat: () => {
				return repeatCount < repeatCountMax;	
			});
		}
	}
}

